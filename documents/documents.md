# 关于std::move 的思考
1. 一般类型的数据如doble，int，char，bool类型是不支持move语义的，只有定义了move特性的`类`才支持移动语义
1. move时也只是最外层的move，如`std::vector<std::vector<int>> a`中,对于`std::move(a)`也只是将最外层的vector的`begin`，`end`,`capacity` **三个指针进行赋值，更内层的则不管** 
1. 即只有有`移动赋值`和`移动构造`函数的类才能有异动语义 
1. 对于push_back(``)时，举例说明如下：
- [ ] 一维vector 进行push_back到二维时：
    - 二维vector的capacity未满，则直接这样：假如一维为右值，则只需要调用vector的右值形式的移动构造，否则需要调用普通形式的拷贝构造;
    - 假如二维vector已经满了，则构造新的位置，再如上面判断一维vector是否支持移动构造
- [ ] 将double 形式push_back一维vector时，capacity未满，则直接构造，满了也扩张后构造，不存在移动语义问题
- [ ] 用move(4.3)试一试是否支持移语义
- [ ]  **看有没有提高效率要看被move的元素支不支持move** ，比如double不支持就没用，vector支持就很有可能提高效率。